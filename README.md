# api-spatcher: [OpenAPI 3.0] API tester
This program allows you to test an API to make sure it matches the specification from an [OpenAPI 3.0] document.
It parses the file and goes through each example in each path and sends requests to the given server over HTTP.   
Thanks to this [OpenAPI 3.0 parser] JSON and YAML are supported.  
api-spatcher stands for API specification matcher if that is interesting to anyone.

## Installation
```sh
go get gitlab.com/ZakCodes/api-spatcher
```

## Usage
### Testing an API
As mentioned, you can use this package to test a running API to make sure it matches a given specification.  
All you have to do for this test is execute the program and give it the path to the `yaml` or `json` spec file and the server:
```sh
api-spatcher api.yaml running-api.example.com:8000
```

### Testing your OpenAPI file  
By using [apisprout], you can create a mock server that matches some given specifications.  
You can then use this tool to test the mock server which will make sure that your request examples match your request schemas and that your response examples match your response schemas.   
Here's an example of how to do:
```sh
port=8000      # localhost port
spec= api.yaml # OpenAPI 3.0 specification file

# Create the mock server in the background.
apisprout --port $port $spec --validate-server &

# You may have to add a delay here

# Start the tool.
api-spatcher $spec http://localhost:$port

# Kill the server
kill $!
```

## Issues
Currently, not much is implemented for this. I will implement things when they are necessary for my projects or when
someone makes a request for it. Don't hesitate it will be a pleasure for me to do it to the limit of my abilities.

## Todo
A whole lot, but here are some things that would be interesting:
- Write some tests
- Test all the different possible parameter combinations
- Verify if the server ends with a `/` before adding the URI to it. Currently both `/` are sent in the request which is problematic.
- Format all the URL parameters and not only the ones in brackets like `{id}`
- Output the result and the progression of the tests as they are going on to a file and/or to the stdout. At least the path, the parameters, the method and the resulting status code should be shown.
- Don't crash the program after a failed test
- Modify the OpenAPI specification to add an expected error code to a request body content example
- Test wrong values to make sure you get an error (this one isn't really necessary because as long as you get success code when you send the right request, then everything is probably fine)

## Thanks
Here are some people/organizations that I want to thank for this project:
- [getkin](https://github.com/getkin/kin-openapi) for the [OpenAPI 3.0 parser]
- [Daniel G. Taylor](https://github.com/danielgtaylor) for his [apisprout] that inspired this project
- [Steve Francia](https://github.com/spf13) for the [Cobra command line tool](https://github.com/spf13/cobra) that is being used in this project   

## License
Copyright &copy; 2019 Zakary Kamal Ismail

<!-- recurrent links -->
[OpenAPI 3.0]: https://www.openapis.org/
[OpenAPI 3.0 parser]: https://github.com/getkin/kin-openapi
[apisprout]: https://github.com/danielgtaylor/apisprout
