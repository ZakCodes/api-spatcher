package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"

	"gopkg.in/yaml.v2"
)

const Version string = "0.1.0"

var (
	// ErrCannotMarshal is set when an example cannot be marshalled.
	ErrCannotMarshal = errors.New("Cannot marshal example")
)

const URLArg = "url"

func main() {
	// Get the filename of the executed binary.
	cmd := filepath.Base(os.Args[0])

	// Create the command.
	root := &cobra.Command {
		Use: fmt.Sprintf("%s [flags] FILE SERVER", cmd),
		Version: Version,
		Args: cobra.MinimumNArgs(2),
		Run: func (cmd *cobra.Command, args []string) {
			// Gets the file and the server from the args and passes them to _client
			// Prints the message of the error returned from _client if there is one
			if err := client(args[0], args[1]) ; err != nil {
				log.Fatal(err.Error())
			}
		},
		Example: fmt.Sprintf("# Basic usage\n  %s openapi.yaml http://localhost:8000", cmd),
	}
	// Execute the command.
	root.Execute()
}

// Common data to all requests
type RequestData struct {
	swagger *openapi3.Swagger
	server string
	client *http.Client
	op *openapi3.Operation
	path string
	method string
}

// Parses the paths from the file and sends a request for each
// example of each endpoint to the server
func client(file string, server string) error {
	// Parse the file.
	swagger, err := openapi3.NewSwaggerLoader().LoadSwaggerFromFile(file)
	if err != nil {
		return err
	}

	data := RequestData {
		swagger: swagger,
		server: server,
		client: &http.Client {},
		op: nil,
		path: "",
		method: "",
	}

	// Go through each path.
	for pathname, path := range swagger.Paths {
		// Format the path with the parameters of the path
		pathname = FormatPath(path.Parameters, pathname)

		// For each method.
		methods := map[string]*openapi3.Operation {
			"GET": path.Get,
			"HEAD": path.Head,
			"POST": path.Post,
			"PUT": path.Put,
			"DELETE": path.Delete,
			"CONNECT": path.Connect,
			"OPTIONS": path.Options,
			"TRACE": path.Trace,
			"PATCH": path.Patch,
		}
		for method, op := range methods {
			if op != nil {
				data.method = method

				// Format the parameters for the given method.
				data.path = FormatPath(op.Parameters, pathname)

				if op.RequestBody != nil && op.RequestBody.Value != nil {
					if !op.RequestBody.Value.Required {
						// Create the request.
						if err := RequestAndValidate(&data); err != nil {
							return err
						}
					}

					for mediatype, content := range op.RequestBody.Value.Content {
						examples := make([]interface{}, 0)

						// Add the example.
						if content.Example != nil {
							examples = append(examples, content.Example)
						}

						// Add the schema example.
						if content.Schema != nil {
							if content.Schema.Value != nil {
								if content.Schema.Value.Example != nil {
									examples = append(examples, content.Schema.Value.Example)
								}
							}
						}

						// Add the examples.
						for _, example := range content.Examples {
							examples = append(examples, example)
						}

						for _, example := range examples {
							encoded, err := EncodeExample(example, mediatype)
							if err != nil {
								return err
							}
							if err := RequestAndValidateBody(&data, mediatype, encoded) ; err != nil {
								return err
							}
						}
					}
				} else {
					if err := RequestAndValidate(&data); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}

// Makes a request with a body and validate the returned response
func RequestAndValidateBody(data *RequestData, mediatype string, body []byte) error {
	req, err := http.NewRequest(data.method, data.server + data.path, bytes.NewReader(body))
	req.Header.Set("Content-Type", mediatype)

	if err != nil {
		return err
	}
	return SendAndValidate(data, req)
}

// Makes a request without a body and validate the returned response
func RequestAndValidate(data *RequestData) error {
	req, err := http.NewRequest(data.method, data.server + data.path, nil)

	if err != nil {
		return err
	}
	return SendAndValidate(data, req)
}

// Sends a request and validates it's body
func SendAndValidate(data *RequestData, req *http.Request) error {
	// Send the request.
	resp, err := data.client.Do(req)
	if err != nil {
		return err
	}
	// Validate the response.
	return ValidateResponse(data, resp, req)
}

// Validates the response of a request
func ValidateResponse(data *RequestData, resp *http.Response, req *http.Request) error {
	router := openapi3filter.NewRouter().WithSwagger(data.swagger)
	route, params, err := router.FindRoute(req.Method, req.URL)
	if err != nil {
		return err
	}

	input := openapi3filter.ResponseValidationInput{
		RequestValidationInput: &openapi3filter.RequestValidationInput {
			Request: req,
			PathParams: params,
			QueryParams: url.Values {},
			Route: route,
			Options: nil,
		},
		Status: resp.StatusCode,
		Header: resp.Header,
		Body: resp.Body,
	}

	return openapi3filter.ValidateResponse(nil, &input)
}

// Encode an example in bytes
// Supported formats: string, []byte, application/json", "application/vnd.api+json",
// "application/x-yaml", "application/yaml", "text/x-yaml", "text/yaml", "text/vnd.yaml",
func EncodeExample(example interface{}, mediatype string) ([]byte, error) {
	var encoded []byte
	var err error

	if s, ok := example.(string); ok {
		encoded = []byte(s)
	} else if _, ok := example.([]byte); ok {
		encoded = example.([]byte)
	} else {
		switch mediatype {
		case "application/json", "application/vnd.api+json":
			encoded, err = json.MarshalIndent(example, "", "  ")
		case "application/x-yaml", "application/yaml", "text/x-yaml", "text/yaml", "text/vnd.yaml":
			encoded, err = yaml.Marshal(example)
		default:
			log.Printf("Cannot marshal as '%s'!", mediatype)
			err = ErrCannotMarshal
		}
	}

	return encoded, err
}

// Formats the path with the examples of the given parameters
// Currently only works
func FormatPath(parameters openapi3.Parameters, path string) string {
	for _, param := range parameters {
		if param.Value.In == "path" {
			if s, ok := param.Value.Example.(string); ok {
				path = strings.Replace(path, "{" + param.Value.Name + "}", s, 1)
			}
		}
	}
	return path
}
